﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class AdminAbsensi
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.PanelHome2 = New System.Windows.Forms.Panel()
        Me.PanelEmployee2 = New System.Windows.Forms.Panel()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.Panel6 = New System.Windows.Forms.Panel()
        Me.Panel7 = New System.Windows.Forms.Panel()
        Me.btPromo = New System.Windows.Forms.Button()
        Me.PanelLogout = New System.Windows.Forms.Panel()
        Me.btLogout = New System.Windows.Forms.Button()
        Me.PanelPassword = New System.Windows.Forms.Panel()
        Me.btAccount = New System.Windows.Forms.Button()
        Me.PanelMember = New System.Windows.Forms.Panel()
        Me.btMember = New System.Windows.Forms.Button()
        Me.PanelMenu = New System.Windows.Forms.Panel()
        Me.btMenu = New System.Windows.Forms.Button()
        Me.PanelEmployee = New System.Windows.Forms.Panel()
        Me.btEmployee = New System.Windows.Forms.Button()
        Me.PanelHome = New System.Windows.Forms.Panel()
        Me.btHome = New System.Windows.Forms.Button()
        Me.namaAdmin = New System.Windows.Forms.Label()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.PictureBox4 = New System.Windows.Forms.PictureBox()
        Me.PictureBox3 = New System.Windows.Forms.PictureBox()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.PictureBox5 = New System.Windows.Forms.PictureBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.Panel5 = New System.Windows.Forms.Panel()
        Me.DataGridView2 = New System.Windows.Forms.DataGridView()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Panel8 = New System.Windows.Forms.Panel()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.btAbsensi = New System.Windows.Forms.Button()
        Me.ComboBox1 = New System.Windows.Forms.ComboBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.TextBox2 = New System.Windows.Forms.TextBox()
        Me.TextBox3 = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.DateTimePicker1 = New System.Windows.Forms.DateTimePicker()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Panel1.SuspendLayout()
        Me.PanelHome2.SuspendLayout()
        Me.PanelEmployee2.SuspendLayout()
        Me.Panel3.SuspendLayout()
        Me.Panel6.SuspendLayout()
        Me.Panel7.SuspendLayout()
        Me.PanelLogout.SuspendLayout()
        Me.PanelPassword.SuspendLayout()
        Me.PanelMember.SuspendLayout()
        Me.PanelMenu.SuspendLayout()
        Me.PanelEmployee.SuspendLayout()
        Me.PanelHome.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel4.SuspendLayout()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox5, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel5.SuspendLayout()
        CType(Me.DataGridView2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel8.SuspendLayout()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.Red
        Me.Panel1.Controls.Add(Me.PictureBox4)
        Me.Panel1.Controls.Add(Me.PictureBox3)
        Me.Panel1.Controls.Add(Me.PictureBox2)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(1017, 17)
        Me.Panel1.TabIndex = 0
        '
        'PanelHome2
        '
        Me.PanelHome2.BackColor = System.Drawing.Color.WhiteSmoke
        Me.PanelHome2.Controls.Add(Me.PanelEmployee2)
        Me.PanelHome2.Controls.Add(Me.Panel3)
        Me.PanelHome2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.PanelHome2.Location = New System.Drawing.Point(0, 17)
        Me.PanelHome2.Name = "PanelHome2"
        Me.PanelHome2.Size = New System.Drawing.Size(1017, 519)
        Me.PanelHome2.TabIndex = 4
        '
        'PanelEmployee2
        '
        Me.PanelEmployee2.BackColor = System.Drawing.Color.White
        Me.PanelEmployee2.Controls.Add(Me.Panel8)
        Me.PanelEmployee2.Controls.Add(Me.Panel5)
        Me.PanelEmployee2.Controls.Add(Me.Panel4)
        Me.PanelEmployee2.Controls.Add(Me.Label2)
        Me.PanelEmployee2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.PanelEmployee2.Location = New System.Drawing.Point(207, 0)
        Me.PanelEmployee2.Name = "PanelEmployee2"
        Me.PanelEmployee2.Size = New System.Drawing.Size(810, 519)
        Me.PanelEmployee2.TabIndex = 4
        '
        'Panel3
        '
        Me.Panel3.BackColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(55, Byte), Integer), CType(CType(80, Byte), Integer))
        Me.Panel3.Controls.Add(Me.Panel6)
        Me.Panel3.Controls.Add(Me.namaAdmin)
        Me.Panel3.Controls.Add(Me.PictureBox1)
        Me.Panel3.Dock = System.Windows.Forms.DockStyle.Left
        Me.Panel3.Font = New System.Drawing.Font("Century Gothic", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Panel3.ForeColor = System.Drawing.Color.White
        Me.Panel3.Location = New System.Drawing.Point(0, 0)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(207, 519)
        Me.Panel3.TabIndex = 1
        '
        'Panel6
        '
        Me.Panel6.Controls.Add(Me.Panel7)
        Me.Panel6.Controls.Add(Me.PanelLogout)
        Me.Panel6.Controls.Add(Me.PanelPassword)
        Me.Panel6.Controls.Add(Me.PanelMember)
        Me.Panel6.Controls.Add(Me.PanelMenu)
        Me.Panel6.Controls.Add(Me.PanelEmployee)
        Me.Panel6.Controls.Add(Me.PanelHome)
        Me.Panel6.Location = New System.Drawing.Point(0, 149)
        Me.Panel6.Name = "Panel6"
        Me.Panel6.Size = New System.Drawing.Size(207, 351)
        Me.Panel6.TabIndex = 3
        '
        'Panel7
        '
        Me.Panel7.BackColor = System.Drawing.Color.FromArgb(CType(CType(10, Byte), Integer), CType(CType(110, Byte), Integer), CType(CType(100, Byte), Integer))
        Me.Panel7.Controls.Add(Me.btPromo)
        Me.Panel7.Location = New System.Drawing.Point(0, 156)
        Me.Panel7.Name = "Panel7"
        Me.Panel7.Size = New System.Drawing.Size(207, 39)
        Me.Panel7.TabIndex = 18
        '
        'btPromo
        '
        Me.btPromo.BackColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(55, Byte), Integer), CType(CType(80, Byte), Integer))
        Me.btPromo.FlatAppearance.BorderSize = 0
        Me.btPromo.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btPromo.Font = New System.Drawing.Font("Century Gothic", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btPromo.Location = New System.Drawing.Point(4, 0)
        Me.btPromo.Name = "btPromo"
        Me.btPromo.Size = New System.Drawing.Size(203, 39)
        Me.btPromo.TabIndex = 4
        Me.btPromo.Text = "Manage Jadwal"
        Me.btPromo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btPromo.UseVisualStyleBackColor = False
        '
        'PanelLogout
        '
        Me.PanelLogout.BackColor = System.Drawing.Color.FromArgb(CType(CType(10, Byte), Integer), CType(CType(110, Byte), Integer), CType(CType(100, Byte), Integer))
        Me.PanelLogout.Controls.Add(Me.btLogout)
        Me.PanelLogout.Location = New System.Drawing.Point(0, 297)
        Me.PanelLogout.Name = "PanelLogout"
        Me.PanelLogout.Size = New System.Drawing.Size(207, 39)
        Me.PanelLogout.TabIndex = 21
        '
        'btLogout
        '
        Me.btLogout.BackColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(55, Byte), Integer), CType(CType(80, Byte), Integer))
        Me.btLogout.FlatAppearance.BorderSize = 0
        Me.btLogout.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btLogout.Font = New System.Drawing.Font("Century Gothic", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btLogout.Location = New System.Drawing.Point(4, 0)
        Me.btLogout.Name = "btLogout"
        Me.btLogout.Size = New System.Drawing.Size(203, 39)
        Me.btLogout.TabIndex = 7
        Me.btLogout.Text = "Logout"
        Me.btLogout.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btLogout.UseVisualStyleBackColor = False
        '
        'PanelPassword
        '
        Me.PanelPassword.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(245, Byte), Integer), CType(CType(215, Byte), Integer))
        Me.PanelPassword.Controls.Add(Me.btAccount)
        Me.PanelPassword.Location = New System.Drawing.Point(0, 250)
        Me.PanelPassword.Name = "PanelPassword"
        Me.PanelPassword.Size = New System.Drawing.Size(207, 39)
        Me.PanelPassword.TabIndex = 20
        '
        'btAccount
        '
        Me.btAccount.BackColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(55, Byte), Integer), CType(CType(80, Byte), Integer))
        Me.btAccount.FlatAppearance.BorderSize = 0
        Me.btAccount.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btAccount.Font = New System.Drawing.Font("Century Gothic", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btAccount.Location = New System.Drawing.Point(4, 0)
        Me.btAccount.Name = "btAccount"
        Me.btAccount.Size = New System.Drawing.Size(203, 39)
        Me.btAccount.TabIndex = 7
        Me.btAccount.Text = "Account Detail"
        Me.btAccount.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btAccount.UseVisualStyleBackColor = False
        '
        'PanelMember
        '
        Me.PanelMember.BackColor = System.Drawing.Color.FromArgb(CType(CType(10, Byte), Integer), CType(CType(110, Byte), Integer), CType(CType(100, Byte), Integer))
        Me.PanelMember.Controls.Add(Me.btMember)
        Me.PanelMember.Location = New System.Drawing.Point(0, 203)
        Me.PanelMember.Name = "PanelMember"
        Me.PanelMember.Size = New System.Drawing.Size(207, 39)
        Me.PanelMember.TabIndex = 19
        '
        'btMember
        '
        Me.btMember.BackColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(55, Byte), Integer), CType(CType(80, Byte), Integer))
        Me.btMember.FlatAppearance.BorderSize = 0
        Me.btMember.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btMember.Font = New System.Drawing.Font("Century Gothic", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btMember.Location = New System.Drawing.Point(4, 0)
        Me.btMember.Name = "btMember"
        Me.btMember.Size = New System.Drawing.Size(203, 39)
        Me.btMember.TabIndex = 5
        Me.btMember.Text = "Manage Laporan"
        Me.btMember.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btMember.UseVisualStyleBackColor = False
        '
        'PanelMenu
        '
        Me.PanelMenu.BackColor = System.Drawing.Color.FromArgb(CType(CType(10, Byte), Integer), CType(CType(110, Byte), Integer), CType(CType(100, Byte), Integer))
        Me.PanelMenu.Controls.Add(Me.btMenu)
        Me.PanelMenu.Location = New System.Drawing.Point(0, 109)
        Me.PanelMenu.Name = "PanelMenu"
        Me.PanelMenu.Size = New System.Drawing.Size(207, 39)
        Me.PanelMenu.TabIndex = 17
        '
        'btMenu
        '
        Me.btMenu.BackColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(55, Byte), Integer), CType(CType(80, Byte), Integer))
        Me.btMenu.FlatAppearance.BorderSize = 0
        Me.btMenu.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btMenu.Font = New System.Drawing.Font("Century Gothic", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btMenu.Location = New System.Drawing.Point(4, 0)
        Me.btMenu.Name = "btMenu"
        Me.btMenu.Size = New System.Drawing.Size(203, 39)
        Me.btMenu.TabIndex = 4
        Me.btMenu.Text = "Manage User"
        Me.btMenu.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btMenu.UseVisualStyleBackColor = False
        '
        'PanelEmployee
        '
        Me.PanelEmployee.BackColor = System.Drawing.Color.FromArgb(CType(CType(10, Byte), Integer), CType(CType(110, Byte), Integer), CType(CType(100, Byte), Integer))
        Me.PanelEmployee.Controls.Add(Me.btEmployee)
        Me.PanelEmployee.Location = New System.Drawing.Point(0, 62)
        Me.PanelEmployee.Name = "PanelEmployee"
        Me.PanelEmployee.Size = New System.Drawing.Size(207, 39)
        Me.PanelEmployee.TabIndex = 16
        '
        'btEmployee
        '
        Me.btEmployee.BackColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(55, Byte), Integer), CType(CType(80, Byte), Integer))
        Me.btEmployee.FlatAppearance.BorderSize = 0
        Me.btEmployee.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btEmployee.Font = New System.Drawing.Font("Century Gothic", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btEmployee.Location = New System.Drawing.Point(4, 0)
        Me.btEmployee.Name = "btEmployee"
        Me.btEmployee.Size = New System.Drawing.Size(203, 39)
        Me.btEmployee.TabIndex = 3
        Me.btEmployee.Text = "Manage Absensi"
        Me.btEmployee.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btEmployee.UseVisualStyleBackColor = False
        '
        'PanelHome
        '
        Me.PanelHome.BackColor = System.Drawing.Color.FromArgb(CType(CType(10, Byte), Integer), CType(CType(110, Byte), Integer), CType(CType(100, Byte), Integer))
        Me.PanelHome.Controls.Add(Me.btHome)
        Me.PanelHome.Location = New System.Drawing.Point(0, 15)
        Me.PanelHome.Name = "PanelHome"
        Me.PanelHome.Size = New System.Drawing.Size(207, 39)
        Me.PanelHome.TabIndex = 15
        '
        'btHome
        '
        Me.btHome.BackColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(55, Byte), Integer), CType(CType(80, Byte), Integer))
        Me.btHome.FlatAppearance.BorderSize = 0
        Me.btHome.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btHome.Font = New System.Drawing.Font("Century Gothic", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btHome.Location = New System.Drawing.Point(4, 0)
        Me.btHome.Name = "btHome"
        Me.btHome.Size = New System.Drawing.Size(203, 39)
        Me.btHome.TabIndex = 2
        Me.btHome.Text = "Home"
        Me.btHome.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btHome.UseVisualStyleBackColor = False
        '
        'namaAdmin
        '
        Me.namaAdmin.Font = New System.Drawing.Font("Century Gothic", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.namaAdmin.Location = New System.Drawing.Point(0, 117)
        Me.namaAdmin.Name = "namaAdmin"
        Me.namaAdmin.Size = New System.Drawing.Size(207, 25)
        Me.namaAdmin.TabIndex = 1
        Me.namaAdmin.Text = "Nama"
        Me.namaAdmin.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = Global.AbsensiSoftwareClub.My.Resources.Resources.boy_8
        Me.PictureBox1.Location = New System.Drawing.Point(58, 24)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(90, 90)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox1.TabIndex = 0
        Me.PictureBox1.TabStop = False
        '
        'PictureBox4
        '
        Me.PictureBox4.Image = Global.AbsensiSoftwareClub.My.Resources.Resources.transparent_button_yellow_3
        Me.PictureBox4.Location = New System.Drawing.Point(975, 2)
        Me.PictureBox4.Name = "PictureBox4"
        Me.PictureBox4.Size = New System.Drawing.Size(12, 12)
        Me.PictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox4.TabIndex = 5
        Me.PictureBox4.TabStop = False
        '
        'PictureBox3
        '
        Me.PictureBox3.Image = Global.AbsensiSoftwareClub.My.Resources.Resources.green_dog_bone_png
        Me.PictureBox3.Location = New System.Drawing.Point(957, 2)
        Me.PictureBox3.Name = "PictureBox3"
        Me.PictureBox3.Size = New System.Drawing.Size(12, 12)
        Me.PictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox3.TabIndex = 4
        Me.PictureBox3.TabStop = False
        '
        'PictureBox2
        '
        Me.PictureBox2.Image = Global.AbsensiSoftwareClub.My.Resources.Resources.button_svg_2
        Me.PictureBox2.Location = New System.Drawing.Point(993, 2)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(12, 12)
        Me.PictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox2.TabIndex = 3
        Me.PictureBox2.TabStop = False
        '
        'Label2
        '
        Me.Label2.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(6, 3)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(792, 43)
        Me.Label2.TabIndex = 5
        Me.Label2.Text = "Absen Siswa"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Panel4
        '
        Me.Panel4.AutoScroll = True
        Me.Panel4.Controls.Add(Me.DataGridView1)
        Me.Panel4.Controls.Add(Me.PictureBox5)
        Me.Panel4.Controls.Add(Me.Label1)
        Me.Panel4.Controls.Add(Me.TextBox1)
        Me.Panel4.Location = New System.Drawing.Point(6, 51)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(295, 449)
        Me.Panel4.TabIndex = 9
        '
        'DataGridView1
        '
        Me.DataGridView1.AllowUserToAddRows = False
        Me.DataGridView1.AllowUserToDeleteRows = False
        Me.DataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.DataGridView1.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.GridColor = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        Me.DataGridView1.Location = New System.Drawing.Point(31, 76)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.ReadOnly = True
        Me.DataGridView1.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders
        Me.DataGridView1.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal
        Me.DataGridView1.Size = New System.Drawing.Size(233, 358)
        Me.DataGridView1.TabIndex = 13
        '
        'PictureBox5
        '
        Me.PictureBox5.Image = Global.AbsensiSoftwareClub.My.Resources.Resources.searching_data_in_database
        Me.PictureBox5.Location = New System.Drawing.Point(266, 36)
        Me.PictureBox5.Name = "PictureBox5"
        Me.PictureBox5.Size = New System.Drawing.Size(26, 27)
        Me.PictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox5.TabIndex = 12
        Me.PictureBox5.TabStop = False
        '
        'Label1
        '
        Me.Label1.Font = New System.Drawing.Font("Century Gothic", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(3, 8)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(289, 25)
        Me.Label1.TabIndex = 11
        Me.Label1.Text = "List Siswa"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'TextBox1
        '
        Me.TextBox1.Location = New System.Drawing.Point(31, 36)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(233, 27)
        Me.TextBox1.TabIndex = 10
        '
        'Panel5
        '
        Me.Panel5.Controls.Add(Me.Button1)
        Me.Panel5.Controls.Add(Me.DataGridView2)
        Me.Panel5.Controls.Add(Me.Label4)
        Me.Panel5.Controls.Add(Me.Label3)
        Me.Panel5.Location = New System.Drawing.Point(308, 51)
        Me.Panel5.Name = "Panel5"
        Me.Panel5.Size = New System.Drawing.Size(260, 449)
        Me.Panel5.TabIndex = 16
        '
        'DataGridView2
        '
        Me.DataGridView2.AllowUserToAddRows = False
        Me.DataGridView2.AllowUserToDeleteRows = False
        Me.DataGridView2.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.DataGridView2.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        Me.DataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView2.GridColor = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        Me.DataGridView2.Location = New System.Drawing.Point(15, 76)
        Me.DataGridView2.Name = "DataGridView2"
        Me.DataGridView2.ReadOnly = True
        Me.DataGridView2.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders
        Me.DataGridView2.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal
        Me.DataGridView2.Size = New System.Drawing.Size(233, 358)
        Me.DataGridView2.TabIndex = 17
        '
        'Label4
        '
        Me.Label4.Font = New System.Drawing.Font("Century Gothic", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(15, 8)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(233, 25)
        Me.Label4.TabIndex = 16
        Me.Label4.Text = "Pilih Siswa"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.Red
        Me.Button1.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.Button1.FlatAppearance.BorderSize = 2
        Me.Button1.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White
        Me.Button1.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.Button1.Font = New System.Drawing.Font("Century Gothic", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.ForeColor = System.Drawing.Color.White
        Me.Button1.Location = New System.Drawing.Point(176, 40)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(72, 27)
        Me.Button1.TabIndex = 17
        Me.Button1.Text = "Hapus"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'Panel8
        '
        Me.Panel8.Controls.Add(Me.Label6)
        Me.Panel8.Controls.Add(Me.DateTimePicker1)
        Me.Panel8.Controls.Add(Me.TextBox3)
        Me.Panel8.Controls.Add(Me.Label8)
        Me.Panel8.Controls.Add(Me.TextBox2)
        Me.Panel8.Controls.Add(Me.Label7)
        Me.Panel8.Controls.Add(Me.Label5)
        Me.Panel8.Controls.Add(Me.btAbsensi)
        Me.Panel8.Controls.Add(Me.ComboBox1)
        Me.Panel8.Location = New System.Drawing.Point(575, 51)
        Me.Panel8.Name = "Panel8"
        Me.Panel8.Size = New System.Drawing.Size(223, 449)
        Me.Panel8.TabIndex = 17
        '
        'Label5
        '
        Me.Label5.Font = New System.Drawing.Font("Century Gothic", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(1, 58)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(217, 25)
        Me.Label5.TabIndex = 18
        Me.Label5.Text = "Pilih Opsi"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'btAbsensi
        '
        Me.btAbsensi.BackColor = System.Drawing.Color.Red
        Me.btAbsensi.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btAbsensi.FlatAppearance.BorderSize = 2
        Me.btAbsensi.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White
        Me.btAbsensi.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White
        Me.btAbsensi.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btAbsensi.ForeColor = System.Drawing.Color.White
        Me.btAbsensi.Location = New System.Drawing.Point(8, 353)
        Me.btAbsensi.Name = "btAbsensi"
        Me.btAbsensi.Size = New System.Drawing.Size(213, 37)
        Me.btAbsensi.TabIndex = 17
        Me.btAbsensi.Text = "Simpan"
        Me.btAbsensi.UseVisualStyleBackColor = False
        '
        'ComboBox1
        '
        Me.ComboBox1.FormattingEnabled = True
        Me.ComboBox1.Items.AddRange(New Object() {"HADIR", "IJIN", "KOSONG"})
        Me.ComboBox1.Location = New System.Drawing.Point(5, 92)
        Me.ComboBox1.Name = "ComboBox1"
        Me.ComboBox1.Size = New System.Drawing.Size(213, 29)
        Me.ComboBox1.TabIndex = 16
        '
        'Label3
        '
        Me.Label3.Font = New System.Drawing.Font("Century Gothic", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(15, 42)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(155, 25)
        Me.Label3.TabIndex = 15
        Me.Label3.Text = "Nama"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label7
        '
        Me.Label7.Font = New System.Drawing.Font("Century Gothic", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(4, 126)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(210, 25)
        Me.Label7.TabIndex = 20
        Me.Label7.Text = "Materi"
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'TextBox2
        '
        Me.TextBox2.Location = New System.Drawing.Point(3, 154)
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.Size = New System.Drawing.Size(211, 27)
        Me.TextBox2.TabIndex = 21
        '
        'TextBox3
        '
        Me.TextBox3.Enabled = False
        Me.TextBox3.Location = New System.Drawing.Point(3, 212)
        Me.TextBox3.Name = "TextBox3"
        Me.TextBox3.Size = New System.Drawing.Size(213, 27)
        Me.TextBox3.TabIndex = 23
        '
        'Label8
        '
        Me.Label8.Font = New System.Drawing.Font("Century Gothic", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(4, 184)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(210, 25)
        Me.Label8.TabIndex = 22
        Me.Label8.Text = "Jumlah Siswa"
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'DateTimePicker1
        '
        Me.DateTimePicker1.Location = New System.Drawing.Point(8, 270)
        Me.DateTimePicker1.Name = "DateTimePicker1"
        Me.DateTimePicker1.Size = New System.Drawing.Size(210, 27)
        Me.DateTimePicker1.TabIndex = 24
        '
        'Label6
        '
        Me.Label6.Font = New System.Drawing.Font("Century Gothic", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(4, 242)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(210, 25)
        Me.Label6.TabIndex = 25
        Me.Label6.Text = "Tanggal"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'AdminAbsensi
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(10.0!, 21.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1017, 536)
        Me.Controls.Add(Me.PanelHome2)
        Me.Controls.Add(Me.Panel1)
        Me.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Margin = New System.Windows.Forms.Padding(5)
        Me.Name = "AdminAbsensi"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Panel1.ResumeLayout(False)
        Me.PanelHome2.ResumeLayout(False)
        Me.PanelEmployee2.ResumeLayout(False)
        Me.Panel3.ResumeLayout(False)
        Me.Panel6.ResumeLayout(False)
        Me.Panel7.ResumeLayout(False)
        Me.PanelLogout.ResumeLayout(False)
        Me.PanelPassword.ResumeLayout(False)
        Me.PanelMember.ResumeLayout(False)
        Me.PanelMenu.ResumeLayout(False)
        Me.PanelEmployee.ResumeLayout(False)
        Me.PanelHome.ResumeLayout(False)
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel4.ResumeLayout(False)
        Me.Panel4.PerformLayout()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox5, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel5.ResumeLayout(False)
        CType(Me.DataGridView2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel8.ResumeLayout(False)
        Me.Panel8.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents PictureBox4 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox3 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox2 As System.Windows.Forms.PictureBox
    Friend WithEvents PanelHome2 As System.Windows.Forms.Panel
    Friend WithEvents PanelEmployee2 As System.Windows.Forms.Panel
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents Panel6 As System.Windows.Forms.Panel
    Friend WithEvents Panel7 As System.Windows.Forms.Panel
    Friend WithEvents btPromo As System.Windows.Forms.Button
    Friend WithEvents PanelLogout As System.Windows.Forms.Panel
    Friend WithEvents btLogout As System.Windows.Forms.Button
    Friend WithEvents PanelPassword As System.Windows.Forms.Panel
    Friend WithEvents btAccount As System.Windows.Forms.Button
    Friend WithEvents PanelMember As System.Windows.Forms.Panel
    Friend WithEvents btMember As System.Windows.Forms.Button
    Friend WithEvents PanelMenu As System.Windows.Forms.Panel
    Friend WithEvents btMenu As System.Windows.Forms.Button
    Friend WithEvents PanelEmployee As System.Windows.Forms.Panel
    Friend WithEvents btEmployee As System.Windows.Forms.Button
    Friend WithEvents PanelHome As System.Windows.Forms.Panel
    Friend WithEvents btHome As System.Windows.Forms.Button
    Friend WithEvents namaAdmin As System.Windows.Forms.Label
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents Panel4 As System.Windows.Forms.Panel
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents PictureBox5 As System.Windows.Forms.PictureBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Panel8 As System.Windows.Forms.Panel
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents btAbsensi As System.Windows.Forms.Button
    Friend WithEvents ComboBox1 As System.Windows.Forms.ComboBox
    Friend WithEvents Panel5 As System.Windows.Forms.Panel
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents DataGridView2 As System.Windows.Forms.DataGridView
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents TextBox3 As System.Windows.Forms.TextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents TextBox2 As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents DateTimePicker1 As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label6 As System.Windows.Forms.Label
End Class
