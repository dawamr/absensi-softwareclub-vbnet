﻿Imports MySql.Data.MySqlClient
Public Class AdminUser

    Private Sub AdminUser_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        LoadData()
    End Sub
    Sub LoadData()
        Try
            laodDataSiswa()
            DataGridView1.DataSource = ds.Tables("ketemu")
            DataGridView1.ReadOnly = True
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub QuerySearch_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles QuerySearch.TextChanged
        Query = QuerySearch.Text
        serachSiswa()
        DataGridView1.DataSource = ds.Tables("ketemu")
        DataGridView1.ReadOnly = True
    End Sub

    Private Sub DataGridView1_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.CellClick
        Try
            If DataGridView1.RowCount > 0 Then
                With DataGridView1
                    baris = .CurrentRow.Index
                    idSiswa = CInt(.Item(0, baris).Value)
                    txtname.Text = CStr(.Item(1, baris).Value)
                    txtemail.Text = CStr(.Item(2, baris).Value)
                    txtPhone.Text = CStr(.Item(4, baris).Value)
                    txtstatus.Text = CStr(.Item(6, baris).Value)
                    path = CStr(.Item(5, baris).Value)
                    pict2.SizeMode = PictureBoxSizeMode.StretchImage
                    pict2.Image = Image.FromFile(path)
                End With
            End If
        Catch ex As Exception
        End Try
    End Sub

    Private Sub btAbsensi_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btAbsensi.Click
        Try
            nameUser = txtname.Text
            emailUser = txtemail.Text
            phoneUser = txtPhone.Text
            statusUser = txtstatus.Text
            AddSiswa()
            LoadData()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
End Class
