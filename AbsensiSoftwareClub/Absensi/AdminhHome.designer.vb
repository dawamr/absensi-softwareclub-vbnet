﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class AdminhHome
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.PanelHome2 = New System.Windows.Forms.Panel()
        Me.PanelEmployee2 = New System.Windows.Forms.Panel()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.Panel6 = New System.Windows.Forms.Panel()
        Me.Panel7 = New System.Windows.Forms.Panel()
        Me.btPromo = New System.Windows.Forms.Button()
        Me.PanelLogout = New System.Windows.Forms.Panel()
        Me.btLogout = New System.Windows.Forms.Button()
        Me.PanelPassword = New System.Windows.Forms.Panel()
        Me.btAccount = New System.Windows.Forms.Button()
        Me.PanelMember = New System.Windows.Forms.Panel()
        Me.btMember = New System.Windows.Forms.Button()
        Me.PanelMenu = New System.Windows.Forms.Panel()
        Me.btMenu = New System.Windows.Forms.Button()
        Me.PanelEmployee = New System.Windows.Forms.Panel()
        Me.btEmployee = New System.Windows.Forms.Button()
        Me.PanelHome = New System.Windows.Forms.Panel()
        Me.btHome = New System.Windows.Forms.Button()
        Me.namaAdmin = New System.Windows.Forms.Label()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.PictureBox4 = New System.Windows.Forms.PictureBox()
        Me.PictureBox3 = New System.Windows.Forms.PictureBox()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        Me.Panel1.SuspendLayout()
        Me.PanelHome2.SuspendLayout()
        Me.Panel3.SuspendLayout()
        Me.Panel6.SuspendLayout()
        Me.Panel7.SuspendLayout()
        Me.PanelLogout.SuspendLayout()
        Me.PanelPassword.SuspendLayout()
        Me.PanelMember.SuspendLayout()
        Me.PanelMenu.SuspendLayout()
        Me.PanelEmployee.SuspendLayout()
        Me.PanelHome.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.Red
        Me.Panel1.Controls.Add(Me.PictureBox4)
        Me.Panel1.Controls.Add(Me.PictureBox3)
        Me.Panel1.Controls.Add(Me.PictureBox2)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(1017, 17)
        Me.Panel1.TabIndex = 0
        '
        'PanelHome2
        '
        Me.PanelHome2.BackColor = System.Drawing.Color.WhiteSmoke
        Me.PanelHome2.Controls.Add(Me.PanelEmployee2)
        Me.PanelHome2.Controls.Add(Me.Panel3)
        Me.PanelHome2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.PanelHome2.Location = New System.Drawing.Point(0, 17)
        Me.PanelHome2.Name = "PanelHome2"
        Me.PanelHome2.Size = New System.Drawing.Size(1017, 519)
        Me.PanelHome2.TabIndex = 4
        '
        'PanelEmployee2
        '
        Me.PanelEmployee2.BackColor = System.Drawing.Color.White
        Me.PanelEmployee2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.PanelEmployee2.Location = New System.Drawing.Point(207, 0)
        Me.PanelEmployee2.Name = "PanelEmployee2"
        Me.PanelEmployee2.Size = New System.Drawing.Size(810, 519)
        Me.PanelEmployee2.TabIndex = 4
        '
        'Panel3
        '
        Me.Panel3.BackColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(55, Byte), Integer), CType(CType(80, Byte), Integer))
        Me.Panel3.Controls.Add(Me.Panel6)
        Me.Panel3.Controls.Add(Me.namaAdmin)
        Me.Panel3.Controls.Add(Me.PictureBox1)
        Me.Panel3.Dock = System.Windows.Forms.DockStyle.Left
        Me.Panel3.Font = New System.Drawing.Font("Century Gothic", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Panel3.ForeColor = System.Drawing.Color.White
        Me.Panel3.Location = New System.Drawing.Point(0, 0)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(207, 519)
        Me.Panel3.TabIndex = 1
        '
        'Panel6
        '
        Me.Panel6.Controls.Add(Me.Panel7)
        Me.Panel6.Controls.Add(Me.PanelLogout)
        Me.Panel6.Controls.Add(Me.PanelPassword)
        Me.Panel6.Controls.Add(Me.PanelMember)
        Me.Panel6.Controls.Add(Me.PanelMenu)
        Me.Panel6.Controls.Add(Me.PanelEmployee)
        Me.Panel6.Controls.Add(Me.PanelHome)
        Me.Panel6.Location = New System.Drawing.Point(0, 149)
        Me.Panel6.Name = "Panel6"
        Me.Panel6.Size = New System.Drawing.Size(207, 351)
        Me.Panel6.TabIndex = 3
        '
        'Panel7
        '
        Me.Panel7.BackColor = System.Drawing.Color.FromArgb(CType(CType(10, Byte), Integer), CType(CType(110, Byte), Integer), CType(CType(100, Byte), Integer))
        Me.Panel7.Controls.Add(Me.btPromo)
        Me.Panel7.Location = New System.Drawing.Point(0, 156)
        Me.Panel7.Name = "Panel7"
        Me.Panel7.Size = New System.Drawing.Size(207, 39)
        Me.Panel7.TabIndex = 18
        '
        'btPromo
        '
        Me.btPromo.BackColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(55, Byte), Integer), CType(CType(80, Byte), Integer))
        Me.btPromo.FlatAppearance.BorderSize = 0
        Me.btPromo.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btPromo.Font = New System.Drawing.Font("Century Gothic", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btPromo.Location = New System.Drawing.Point(4, 0)
        Me.btPromo.Name = "btPromo"
        Me.btPromo.Size = New System.Drawing.Size(203, 39)
        Me.btPromo.TabIndex = 4
        Me.btPromo.Text = "Manage Jadwal"
        Me.btPromo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btPromo.UseVisualStyleBackColor = False
        '
        'PanelLogout
        '
        Me.PanelLogout.BackColor = System.Drawing.Color.FromArgb(CType(CType(10, Byte), Integer), CType(CType(110, Byte), Integer), CType(CType(100, Byte), Integer))
        Me.PanelLogout.Controls.Add(Me.btLogout)
        Me.PanelLogout.Location = New System.Drawing.Point(0, 297)
        Me.PanelLogout.Name = "PanelLogout"
        Me.PanelLogout.Size = New System.Drawing.Size(207, 39)
        Me.PanelLogout.TabIndex = 21
        '
        'btLogout
        '
        Me.btLogout.BackColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(55, Byte), Integer), CType(CType(80, Byte), Integer))
        Me.btLogout.FlatAppearance.BorderSize = 0
        Me.btLogout.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btLogout.Font = New System.Drawing.Font("Century Gothic", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btLogout.Location = New System.Drawing.Point(4, 0)
        Me.btLogout.Name = "btLogout"
        Me.btLogout.Size = New System.Drawing.Size(203, 39)
        Me.btLogout.TabIndex = 7
        Me.btLogout.Text = "Logout"
        Me.btLogout.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btLogout.UseVisualStyleBackColor = False
        '
        'PanelPassword
        '
        Me.PanelPassword.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(245, Byte), Integer), CType(CType(215, Byte), Integer))
        Me.PanelPassword.Controls.Add(Me.btAccount)
        Me.PanelPassword.Location = New System.Drawing.Point(0, 250)
        Me.PanelPassword.Name = "PanelPassword"
        Me.PanelPassword.Size = New System.Drawing.Size(207, 39)
        Me.PanelPassword.TabIndex = 20
        '
        'btAccount
        '
        Me.btAccount.BackColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(55, Byte), Integer), CType(CType(80, Byte), Integer))
        Me.btAccount.FlatAppearance.BorderSize = 0
        Me.btAccount.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btAccount.Font = New System.Drawing.Font("Century Gothic", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btAccount.Location = New System.Drawing.Point(4, 0)
        Me.btAccount.Name = "btAccount"
        Me.btAccount.Size = New System.Drawing.Size(203, 39)
        Me.btAccount.TabIndex = 7
        Me.btAccount.Text = "Account Detail"
        Me.btAccount.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btAccount.UseVisualStyleBackColor = False
        '
        'PanelMember
        '
        Me.PanelMember.BackColor = System.Drawing.Color.FromArgb(CType(CType(10, Byte), Integer), CType(CType(110, Byte), Integer), CType(CType(100, Byte), Integer))
        Me.PanelMember.Controls.Add(Me.btMember)
        Me.PanelMember.Location = New System.Drawing.Point(0, 203)
        Me.PanelMember.Name = "PanelMember"
        Me.PanelMember.Size = New System.Drawing.Size(207, 39)
        Me.PanelMember.TabIndex = 19
        '
        'btMember
        '
        Me.btMember.BackColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(55, Byte), Integer), CType(CType(80, Byte), Integer))
        Me.btMember.FlatAppearance.BorderSize = 0
        Me.btMember.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btMember.Font = New System.Drawing.Font("Century Gothic", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btMember.Location = New System.Drawing.Point(4, 0)
        Me.btMember.Name = "btMember"
        Me.btMember.Size = New System.Drawing.Size(203, 39)
        Me.btMember.TabIndex = 5
        Me.btMember.Text = "Manage Laporan"
        Me.btMember.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btMember.UseVisualStyleBackColor = False
        '
        'PanelMenu
        '
        Me.PanelMenu.BackColor = System.Drawing.Color.FromArgb(CType(CType(10, Byte), Integer), CType(CType(110, Byte), Integer), CType(CType(100, Byte), Integer))
        Me.PanelMenu.Controls.Add(Me.btMenu)
        Me.PanelMenu.Location = New System.Drawing.Point(0, 109)
        Me.PanelMenu.Name = "PanelMenu"
        Me.PanelMenu.Size = New System.Drawing.Size(207, 39)
        Me.PanelMenu.TabIndex = 17
        '
        'btMenu
        '
        Me.btMenu.BackColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(55, Byte), Integer), CType(CType(80, Byte), Integer))
        Me.btMenu.FlatAppearance.BorderSize = 0
        Me.btMenu.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btMenu.Font = New System.Drawing.Font("Century Gothic", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btMenu.Location = New System.Drawing.Point(4, 0)
        Me.btMenu.Name = "btMenu"
        Me.btMenu.Size = New System.Drawing.Size(203, 39)
        Me.btMenu.TabIndex = 4
        Me.btMenu.Text = "Manage User"
        Me.btMenu.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btMenu.UseVisualStyleBackColor = False
        '
        'PanelEmployee
        '
        Me.PanelEmployee.BackColor = System.Drawing.Color.FromArgb(CType(CType(10, Byte), Integer), CType(CType(110, Byte), Integer), CType(CType(100, Byte), Integer))
        Me.PanelEmployee.Controls.Add(Me.btEmployee)
        Me.PanelEmployee.Location = New System.Drawing.Point(0, 62)
        Me.PanelEmployee.Name = "PanelEmployee"
        Me.PanelEmployee.Size = New System.Drawing.Size(207, 39)
        Me.PanelEmployee.TabIndex = 16
        '
        'btEmployee
        '
        Me.btEmployee.BackColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(55, Byte), Integer), CType(CType(80, Byte), Integer))
        Me.btEmployee.FlatAppearance.BorderSize = 0
        Me.btEmployee.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btEmployee.Font = New System.Drawing.Font("Century Gothic", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btEmployee.Location = New System.Drawing.Point(4, 0)
        Me.btEmployee.Name = "btEmployee"
        Me.btEmployee.Size = New System.Drawing.Size(203, 39)
        Me.btEmployee.TabIndex = 3
        Me.btEmployee.Text = "Manage Absensi"
        Me.btEmployee.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btEmployee.UseVisualStyleBackColor = False
        '
        'PanelHome
        '
        Me.PanelHome.BackColor = System.Drawing.Color.FromArgb(CType(CType(10, Byte), Integer), CType(CType(110, Byte), Integer), CType(CType(100, Byte), Integer))
        Me.PanelHome.Controls.Add(Me.btHome)
        Me.PanelHome.Location = New System.Drawing.Point(0, 15)
        Me.PanelHome.Name = "PanelHome"
        Me.PanelHome.Size = New System.Drawing.Size(207, 39)
        Me.PanelHome.TabIndex = 15
        '
        'btHome
        '
        Me.btHome.BackColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(55, Byte), Integer), CType(CType(80, Byte), Integer))
        Me.btHome.FlatAppearance.BorderSize = 0
        Me.btHome.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btHome.Font = New System.Drawing.Font("Century Gothic", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btHome.Location = New System.Drawing.Point(4, 0)
        Me.btHome.Name = "btHome"
        Me.btHome.Size = New System.Drawing.Size(203, 39)
        Me.btHome.TabIndex = 2
        Me.btHome.Text = "Home"
        Me.btHome.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btHome.UseVisualStyleBackColor = False
        '
        'namaAdmin
        '
        Me.namaAdmin.Font = New System.Drawing.Font("Century Gothic", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.namaAdmin.Location = New System.Drawing.Point(0, 117)
        Me.namaAdmin.Name = "namaAdmin"
        Me.namaAdmin.Size = New System.Drawing.Size(207, 25)
        Me.namaAdmin.TabIndex = 1
        Me.namaAdmin.Text = "Nama"
        Me.namaAdmin.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = Global.AbsensiSoftwareClub.My.Resources.Resources.boy_8
        Me.PictureBox1.Location = New System.Drawing.Point(58, 24)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(90, 90)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox1.TabIndex = 0
        Me.PictureBox1.TabStop = False
        '
        'PictureBox4
        '
        Me.PictureBox4.Image = Global.AbsensiSoftwareClub.My.Resources.Resources.transparent_button_yellow_3
        Me.PictureBox4.Location = New System.Drawing.Point(975, 2)
        Me.PictureBox4.Name = "PictureBox4"
        Me.PictureBox4.Size = New System.Drawing.Size(12, 12)
        Me.PictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox4.TabIndex = 5
        Me.PictureBox4.TabStop = False
        '
        'PictureBox3
        '
        Me.PictureBox3.Image = Global.AbsensiSoftwareClub.My.Resources.Resources.green_dog_bone_png
        Me.PictureBox3.Location = New System.Drawing.Point(957, 2)
        Me.PictureBox3.Name = "PictureBox3"
        Me.PictureBox3.Size = New System.Drawing.Size(12, 12)
        Me.PictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox3.TabIndex = 4
        Me.PictureBox3.TabStop = False
        '
        'PictureBox2
        '
        Me.PictureBox2.Image = Global.AbsensiSoftwareClub.My.Resources.Resources.button_svg_2
        Me.PictureBox2.Location = New System.Drawing.Point(993, 2)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(12, 12)
        Me.PictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox2.TabIndex = 3
        Me.PictureBox2.TabStop = False
        '
        'AdminhHome
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(10.0!, 21.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1017, 536)
        Me.Controls.Add(Me.PanelHome2)
        Me.Controls.Add(Me.Panel1)
        Me.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Margin = New System.Windows.Forms.Padding(5)
        Me.Name = "AdminhHome"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Panel1.ResumeLayout(False)
        Me.PanelHome2.ResumeLayout(False)
        Me.Panel3.ResumeLayout(False)
        Me.Panel6.ResumeLayout(False)
        Me.Panel7.ResumeLayout(False)
        Me.PanelLogout.ResumeLayout(False)
        Me.PanelPassword.ResumeLayout(False)
        Me.PanelMember.ResumeLayout(False)
        Me.PanelMenu.ResumeLayout(False)
        Me.PanelEmployee.ResumeLayout(False)
        Me.PanelHome.ResumeLayout(False)
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents PictureBox4 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox3 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox2 As System.Windows.Forms.PictureBox
    Friend WithEvents PanelHome2 As System.Windows.Forms.Panel
    Friend WithEvents PanelEmployee2 As System.Windows.Forms.Panel
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents Panel6 As System.Windows.Forms.Panel
    Friend WithEvents Panel7 As System.Windows.Forms.Panel
    Friend WithEvents btPromo As System.Windows.Forms.Button
    Friend WithEvents PanelLogout As System.Windows.Forms.Panel
    Friend WithEvents btLogout As System.Windows.Forms.Button
    Friend WithEvents PanelPassword As System.Windows.Forms.Panel
    Friend WithEvents btAccount As System.Windows.Forms.Button
    Friend WithEvents PanelMember As System.Windows.Forms.Panel
    Friend WithEvents btMember As System.Windows.Forms.Button
    Friend WithEvents PanelMenu As System.Windows.Forms.Panel
    Friend WithEvents btMenu As System.Windows.Forms.Button
    Friend WithEvents PanelEmployee As System.Windows.Forms.Panel
    Friend WithEvents btEmployee As System.Windows.Forms.Button
    Friend WithEvents PanelHome As System.Windows.Forms.Panel
    Friend WithEvents btHome As System.Windows.Forms.Button
    Friend WithEvents namaAdmin As System.Windows.Forms.Label
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
End Class
